# FTP Console Client

This is a simple and minimalist console FTP client.\
Key features are file uploading and downloading.

# Download

Go to **Tags** and download artifacts

# Requirements

- Java 11 (JRE or JDK)

# How to use it

Download the application and run it with `java -jar ftpcc.jar`. Without any other parameters, the help is automatically displayed.

# Parameters

`-m` = (**required**) The type of method we want to use. Available values: [upload, download]\
`-c` = (**required**) Connection string in format 'userName:password@host:port'. The shortest format can be: 'userName@host'. For example: 'admin:pass@domain.com:5021' or 'admin@domain.com' with default port 21 and without password.\
`-s` = Source or local file. It is required to upload the file to the server.\
`-d` = Destination file.\
`-bs` = Custom buffer size for uploading and downloading files. The size of the buffer should be increased by file size. For small files, just use 8128. For large files from hundreds of MB use at least 32768. By default, dynamic buffer calculation is used.\
`-ft` = File type. Ensures the correct file transfer type. Available values: [binary, ascii, ebcdic, local]\
`-tm` = Transfer mode. Available values: [stream, compressed, block]\
`-fs` = File structure. It is used for specific purposes. Available values: [file, page, record]\
`--charset` = Custom charset. Default is UTF-8.\
`--slim` = Enables slim mode. In this mode, only error messages and progress are displayed.\
`--silent` = Silent mode. Only potential errors are written to the console.\
`--passive` = Enable or disable passive mode. The default value is True.\
`--help` = Displays this help.

# Hidden features

- If the output file name ends with a slash, it is the directory in which it copies the file with the original name.
  - example: `-s file.txt -d data/` make `-s file.txt -d data/file.txt`
  
- The output parameter `-d` supports the variables:
  - **[name]** = Adds the name of the original file. No file extension
  - **[chinese-name]** = Generates a random name that contains only Chinese historical characters. The length will be
   according to the length of the original name * 2 (example: `셙짎였쇆뿦짰룜맀삆릟쓫삽랃`.exe)
  - **[base64-name]** = Converts file name to base64 (example: "my file" to `bXkgZmlsZS5leGU=`)
  - **[upper-name]** = Converts the file name to uppercase
  - **[lower-name]** = Converts the file name to lowercase
  - **[url-encode-name]** = Encode the file name so it can be used in the URL (example: "file-kouř" to `file-kou%C5%99`)
  - **[ext]** = Returns the file extension
  - **[lower-ext]** = Converts the file extension to lowercase
  - **[uuid]** = It generates a totally unique hash (example: `f4af77fcd27f4ee180bed783ce55b845`)
  - **[mtime]** = Gets the current time in milliseconds (example: `1547495566359`)
  - **[date]** = Inserts the current date in the format: yyyy-MM-dd (example: `2019-02-16`)
  - **[datetime]** = Inserts the current date, including the time in the format: yyyy-MM-dd_HH-mm-ss (example: 
  `2019-02-16_20-45-32`)
  - **[rand-number]** = Generates a random 6-digit number
  
> Example: `-s image.jpg -d "{uuid}.{ext}"` will create something like f4af77fcd27f4ee180bed783ce55b845.jpg

# Few examples

**Upload 'file.txt' file to domain.com**\
`java -jar ftpcc.jar -m upload -c admin:password@domain.com -s c:/file.txt`

**Switch off passive mode**\
`java -jar ftpcc.jar -m upload -c admin:password@domain.com -s c:/file.txt --passive false`

**Upload 'file.txt' file to domain.com under a different name**\
`java -jar ftpcc.jar -m upload -c admin:password@domain.com -s c:/file.txt -d "my file.txt"`

**Upload 'my file.txt' file to domain.com into another name and directory (does not matter if it does not exist)**\
`java -jar ftpcc.jar -m upload -c admin:password@domain.com -s "c:/my file.txt" -d "custom/files/my super file.txt"`

**Sample of advanced settings. It uploads 'file.txt' (1gb) to the server under the name 'data/demo.txt'. It uses the -Z compression mode, file structure and will send the file to 5MB fragments**\
`java -jar ftpcc.jar -m upload -c admin:password@domain.com -s c:/file.txt -d data/demo.txt -bs 5000000 -ft binary -tm compressed -fs file`

**Download the file from FTP and save it to the same directory as ftpcc under the same name as FTP**\
`java -jar ftpcc.jar -m download -c admin:password@domain.com -s file.txt`

**Download the file and save it to another directory**\
`java -jar ftpcc.jar -m download -c admin:password@domain.com -s "some dir/my/file 1.txt" -d "c:/data/file 1.txt"`

> Everything that can be used for uploading can be used for download. The only difference is another method.