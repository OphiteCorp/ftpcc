package oc.mimic.ftpcc.util;

import java.text.MessageFormat;

/**
 * Pomocné metody.
 *
 * @author mimic
 */
public final class Utils {

  /**
   * Naformátuje progres jako Od/Do, který má pevnou šířku.
   *
   * @param fileSize Celková velikost souboru.
   * @param progress Aktální stav velikosti souboru.
   *
   * @return Formátovaný progres.
   */
  public static String formatProgress(long fileSize, long progress) {
    var sizeStr = humanReadableFileSize(fileSize);
    var uplStr = humanReadableFileSize(progress);
    var percent = ((100. / fileSize) * progress) / 100.;

    var max = Math.max(sizeStr.length(), uplStr.length());
    var min = Math.min(sizeStr.length(), uplStr.length());
    uplStr = " ".repeat(max - min) + uplStr;

    return MessageFormat.format("# {0} / {1} >> {2,number,#.###%}", uplStr, sizeStr, percent);
  }

  /**
   * Převede velikost souboru na čitelný formát.
   *
   * @param fileSize Velikost souboru v bytech.
   *
   * @return Formátovaná velikost souboru.
   */
  public static String humanReadableFileSize(long fileSize) {
    return humanReadableFileSize(fileSize, false, 3);
  }

  /**
   * Převede velikost souboru na čitelný formát.
   *
   * @param fileSize Velikost souboru v bytech.
   * @param binary   Binární výstup?
   * @param accuracy Přesnost na desetinná místa.
   *
   * @return Formátovaná velikost souboru.
   */
  public static String humanReadableFileSize(long fileSize, boolean binary, int accuracy) {
    var unit = binary ? 1024 : 1000;
    if (fileSize < unit) {
      return fileSize + " B";
    }
    var exp = (int) (Math.log(fileSize) / Math.log(unit));
    var pre = (binary ? "KMGTPE" : "kMGTPE").charAt(exp - 1) + (binary ? "i" : "");
    return String.format("%." + accuracy + "f %sB", fileSize / Math.pow(unit, exp), pre);
  }

  /**
   * Vypočte vhodnou velikost bufferu podle velikosti souboru.
   *
   * @param fileSize Velikost souboru v bytech.
   *
   * @return Velikost bufferu.
   */
  public static int calculateBufferSize(long fileSize) {
    final long _1m = 1024 * 1024;
    final long _2m = _1m * 2;
    final long _5m = _1m * 5;
    final long _10m = _1m * 10;
    final long _50m = _1m * 50;
    final long _100m = _1m * 100;
    final long _1000m = _1m * 1000; // 1gb

    if (fileSize < _1m) {
      return 1024;
    }
    if (fileSize < _2m) {
      return 2048;
    }
    if (fileSize < _5m) {
      return 4096;
    }
    if (fileSize < _10m) {
      return 8192;
    }
    if (fileSize < _50m) {
      return 16384;
    }
    if (fileSize < _100m) {
      return 32768;
    }
    if (fileSize < _1000m) {
      return 65536;
    }
    return 131072; // pro 1gb soubory a větší
  }

  /**
   * Odebere příponu souboru z jeho názvu.
   *
   * @param fileName Název souboru.
   *
   * @return Název souboru bez přípony.
   */
  public static String removeFileExtension(String fileName) {
    if (fileName.lastIndexOf('.') != -1) {
      return fileName.substring(0, fileName.lastIndexOf('.'));
    }
    return fileName;
  }

  /**
   * Získá příponu souboru.
   *
   * @param fileName Vstupní název souboru.
   *
   * @return Přípona souboru.
   */
  public static String getFileExtension(String fileName) {
    if (fileName == null) {
      return null;
    }
    var lastIndexOf = fileName.lastIndexOf(".");

    if (lastIndexOf == -1) {
      return "";
    }
    var ext = fileName.substring(lastIndexOf);

    if (ext.startsWith(".")) {
      ext = ext.substring(1);
    }
    return ext;
  }
}
