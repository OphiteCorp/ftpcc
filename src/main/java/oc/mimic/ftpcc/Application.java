package oc.mimic.ftpcc;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import oc.mimic.ftpcc.client.DownloadClient;
import oc.mimic.ftpcc.client.UploadClient;
import oc.mimic.ftpcc.client.config.ClientConfig;
import oc.mimic.ftpcc.client.config.CommonConfig;
import oc.mimic.ftpcc.client.config.DownloadClientConfig;
import oc.mimic.ftpcc.client.config.UploadClientConfig;
import oc.mimic.ftpcc.param.Params;
import oc.mimic.ftpcc.util.Logger;

/**
 * Hlavní třída aplikace.
 *
 * @author mimic
 */
public final class Application {

  private static final String VERSION = "1.2";

  public static void main(String... args) {
    var params = new Params();
    var cmd = JCommander.newBuilder().addObject(params).build();
    var validParams = false;

    if (args.length > 0) {
      try {
        cmd.parse(args);
        validParams = true;
      } catch (ParameterException e) {
        Logger.error(e.getMessage());
      }
      if (params.isHelp()) {
        cmd.usage();
      }
    } else {
      cmd.usage();
    }
    Logger.slimMode = (params.isSlim() || params.isSilent());

    if (validParams && !params.isHelp()) {
      if (!params.isSlim() && !params.isSilent()) {
        Logger.info("Starting...");
      }
      var commonConfig = new CommonConfig();
      commonConfig.setSlimMode(params.isSlim());
      commonConfig.setSilentMode(params.isSilent());

      var config = new ClientConfig(params.getConnectionConfig());
      config.setBufferSize(params.getBufferSize());
      config.setCharset(params.getCharset());
      config.setFileType(params.getFileType());
      config.setTransferMode(params.getTransferMode());
      config.setFileStructure(params.getFileStructure());
      config.setPassiveMode(params.isPassive());

      switch (params.getMethod()) {
        case upload:
          var uClient = new UploadClient();
          uClient.silentMode(params.isSilent());
          try {
            var conf = new UploadClientConfig(config);
            conf.setCommon(commonConfig);
            uClient.upload(conf, params.getSource(), params.getDestination());
            uClient.logout();
          } catch (Exception e) {
            Logger.error("An unexpected error has occurred and the application will be terminated");
            e.printStackTrace();
          } finally {
            uClient.closeConnection();
          }
          break;

        case download:
          var dClient = new DownloadClient();
          dClient.silentMode(params.isSilent());
          try {
            var conf = new DownloadClientConfig(config);
            conf.setCommon(commonConfig);
            dClient.download(conf, params.getSource(), params.getDestination());
            dClient.logout();
          } catch (Exception e) {
            Logger.error("An unexpected error has occurred and the application will be terminated");
            e.printStackTrace();
          } finally {
            dClient.closeConnection();
          }
          break;
      }
    }
    if (!params.isSlim() && !params.isSilent()) {
      Logger.info("Application FTP Console Client v{0} (by mimic | 2019) was terminated", VERSION);
    }
  }
}
