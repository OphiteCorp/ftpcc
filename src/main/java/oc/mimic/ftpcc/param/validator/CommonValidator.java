package oc.mimic.ftpcc.param.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import oc.mimic.ftpcc.param.Params;
import oc.mimic.ftpcc.param.type.FileStructureType;
import oc.mimic.ftpcc.param.type.FileType;
import oc.mimic.ftpcc.param.type.MethodType;
import oc.mimic.ftpcc.param.type.TransferModeType;

import java.nio.charset.Charset;

/**
 * Běžný validátor pro ostatní nespecifický vstupní parametry.
 *
 * @author mimic
 */
public final class CommonValidator implements IParameterValidator {

  private static final int BUFFER_MIN = 128;
  private static final int BUFFER_MAX = 134217728;

  @Override
  public void validate(String name, String value) throws ParameterException {
    String msg = null;

    switch (name) {
      case Params.METHOD:
        if (!isValidMethod(value)) {
          msg = "Invalid method: '" + value + "'";
        }
        break;

      case Params.FILE_TYPE:
        if (!isValidFileType(value)) {
          msg = "Invalid file type: '" + value + "'";
        }
        break;

      case Params.TRANSFER_MODE:
        if (!isValidTransferModeType(value)) {
          msg = "Invalid transfer mode: '" + value + "'";
        }
        break;

      case Params.FILE_STRUCTURE:
        if (!isValidFileStructureType(value)) {
          msg = "Invalid file structure: '" + value + "'";
        }
        break;

      case Params.BUFFER_SIZE:
        if (!isValidBufferSize(value)) {
          msg = "Invalid buffer size: '" + value + "'. The size range is: " + getBufferSizeLimit();
        }
        break;

      case Params.CHARSET:
        if (!isValidCharset(value)) {
          msg = "Invalid charset: '" + value + "'. Supported charsets are: " + getSupportedCharsets();
        }
        break;
    }
    if (msg != null) {
      throw new ParameterException(msg);
    }
  }

  private static boolean isValidMethod(String value) {
    try {
      MethodType.valueOf(value.toLowerCase());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidFileType(String value) {
    try {
      FileType.valueOf(value.toLowerCase());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidTransferModeType(String value) {
    try {
      TransferModeType.valueOf(value.toLowerCase());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidFileStructureType(String value) {
    try {
      FileStructureType.valueOf(value.toLowerCase());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidBufferSize(String value) {
    try {
      var val = Integer.parseInt(value);
      return (val >= BUFFER_MIN && val <= BUFFER_MAX);
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidCharset(String value) {
    try {
      return Charset.isSupported(value);
    } catch (Exception e) {
      return false;
    }
  }

  private static String getSupportedCharsets() {
    return String.join(", ", Charset.availableCharsets().keySet());
  }

  private static String getBufferSizeLimit() {
    return String.format("%s-%s", BUFFER_MIN, BUFFER_MAX);
  }
}
