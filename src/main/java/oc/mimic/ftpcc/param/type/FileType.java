package oc.mimic.ftpcc.param.type;

import org.apache.commons.net.ftp.FTP;

/**
 * Typ souboru pro přenos.
 *
 * @author mimic
 */
public enum FileType {

    binary(FTP.BINARY_FILE_TYPE),
    ascii(FTP.ASCII_FILE_TYPE),
    ebcdic(FTP.EBCDIC_FILE_TYPE),
    local(FTP.LOCAL_FILE_TYPE);

    private final int value;

    FileType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
