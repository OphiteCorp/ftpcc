package oc.mimic.ftpcc.param.type;

import org.apache.commons.net.ftp.FTP;

/**
 * Typ struktůry souboru.
 *
 * @author mimic
 */
public enum FileStructureType {

  file(FTP.FILE_STRUCTURE),
  page(FTP.PAGE_STRUCTURE),
  record(FTP.RECORD_STRUCTURE);

  private final int value;

  FileStructureType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
