package oc.mimic.ftpcc.param.type;

/**
 * Typ metody.
 *
 * @author mimic
 */
public enum MethodType {

  /**
   * Nahrání souboru na server.
   */
  upload,

  /**
   * Stáhne soubor ze serveru.
   */
  download
}
