package oc.mimic.ftpcc.param.type;

import org.apache.commons.net.ftp.FTP;

/**
 * Typ transféru souboru.
 *
 * @author mimic
 */
public enum TransferModeType {

  stream(FTP.STREAM_TRANSFER_MODE),
  compressed(FTP.COMPRESSED_TRANSFER_MODE),
  block(FTP.BLOCK_TRANSFER_MODE);

  private final int value;

  TransferModeType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
