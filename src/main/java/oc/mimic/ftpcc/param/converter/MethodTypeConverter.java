package oc.mimic.ftpcc.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.ftpcc.param.type.MethodType;

/**
 * Converter pro parametr metody.
 *
 * @author mimic
 */
public final class MethodTypeConverter implements IStringConverter<MethodType> {

  @Override
  public MethodType convert(String value) {
    return MethodType.valueOf(value.toLowerCase());
  }
}
