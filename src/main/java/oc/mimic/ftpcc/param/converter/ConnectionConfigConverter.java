package oc.mimic.ftpcc.param.converter;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;
import oc.mimic.ftpcc.client.config.ConnectionConfig;

/**
 * Converter, který převede připojovací řetězec do {@link ConnectionConfig}.
 *
 * @author mimic
 */
public final class ConnectionConfigConverter implements IStringConverter<ConnectionConfig> {

  @Override
  public ConnectionConfig convert(String value) {
    // value: jmeno:heslo@host:port | jmeno:heslo@host | jmeno @ host | jmeno@host:port
    try {
      value = value.replaceAll(" ", "");
      var at = value.indexOf("@");

      if (at > 0) {
        String userName = null;
        String password = null;
        String host = null;
        Integer port = null;
        var parts = value.split("@");

        if (parts.length == 2) {
          var colon = parts[0].indexOf(":");

          if (colon == -1) {
            userName = parts[0];
          } else {
            var colons = parts[0].split(":");

            if (colons.length == 1) {
              userName = colons[0];

            } else if (colons.length == 2) {
              userName = colons[0];
              password = colons[1];
            }
          }
          colon = parts[1].indexOf(":");

          if (colon == -1) {
            host = parts[1];
          } else {
            var colons = parts[1].split(":");

            if (colons.length == 2) {
              host = colons[0];
              port = Integer.parseInt(colons[1]);
            }
          }
        }
        if (userName == null || host == null) {
          throw new ParameterException("The user name and server address are required in the connection string");
        }
        return createConnectionConfig(userName, password, host, port);
      }
    } catch (Exception e) {
      // nic
    }
    throw new ParameterException("Invalid connection string");
  }

  private static ConnectionConfig createConnectionConfig(String userName, String password, String host, Integer port) {

    var config = new ConnectionConfig();
    config.setUserName(userName);
    config.setHost(host);

    if (password != null) {
      config.setPassword(password);
    }
    if (port != null) {
      config.setPort(port);
    }
    return config;
  }
}
