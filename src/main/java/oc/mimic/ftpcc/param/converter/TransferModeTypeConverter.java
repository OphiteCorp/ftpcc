package oc.mimic.ftpcc.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.ftpcc.param.type.TransferModeType;

/**
 * Converter pro parametr typu transfer módu.
 *
 * @author mimic
 */
public final class TransferModeTypeConverter implements IStringConverter<TransferModeType> {

  @Override
  public TransferModeType convert(String value) {
    return TransferModeType.valueOf(value.toLowerCase());
  }
}
