package oc.mimic.ftpcc.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.ftpcc.param.type.FileType;

/**
 * Converter pro parametr typu souboru.
 *
 * @author mimic
 */
public final class FileTypeConverter implements IStringConverter<FileType> {

  @Override
  public FileType convert(String value) {
    return FileType.valueOf(value.toLowerCase());
  }
}
