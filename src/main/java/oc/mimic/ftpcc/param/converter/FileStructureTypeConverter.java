package oc.mimic.ftpcc.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.ftpcc.param.type.FileStructureType;

/**
 * Converter pro parametr typu struktůry souboru.
 *
 * @author mimic
 */
public final class FileStructureTypeConverter implements IStringConverter<FileStructureType> {

  @Override
  public FileStructureType convert(String value) {
    return FileStructureType.valueOf(value.toLowerCase());
  }
}
