package oc.mimic.ftpcc.param;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.BooleanConverter;
import com.beust.jcommander.converters.IntegerConverter;
import oc.mimic.ftpcc.client.config.ClientConfig;
import oc.mimic.ftpcc.client.config.ConnectionConfig;
import oc.mimic.ftpcc.param.converter.*;
import oc.mimic.ftpcc.param.type.FileStructureType;
import oc.mimic.ftpcc.param.type.FileType;
import oc.mimic.ftpcc.param.type.MethodType;
import oc.mimic.ftpcc.param.type.TransferModeType;
import oc.mimic.ftpcc.param.validator.CommonValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Parametry aplikace.
 *
 * @author mimic
 */
@Parameters(commandDescription = "Available application parameters")
public final class Params {

  public static final String METHOD = "-m";
  public static final String CONNETION_STRING = "-c";
  public static final String SOURCE = "-s";
  public static final String DESTINATION = "-d";
  public static final String FILE_TYPE = "-ft";
  public static final String TRANSFER_MODE = "-tm";
  public static final String FILE_STRUCTURE = "-fs";
  public static final String BUFFER_SIZE = "-bs";
  public static final String CHARSET = "--charset";
  public static final String SLIM = "--slim";
  public static final String SILENT = "--silent";
  public static final String PASSIVE = "--passive";
  public static final String HELP = "--help";

  @Parameter
  private List<String> parameters = new ArrayList<>(); // obsahuje všechny ostatní parametry, které nejsou podporované

  @Parameter(names = METHOD,
             order = 1,
             required = true,
             converter = MethodTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "The type of method we want to use.")
  private MethodType method;

  @Parameter(names = CONNETION_STRING,
             required = true,
             order = 2,
             converter = ConnectionConfigConverter.class,
             description = "Connection string in format: 'userName:password@host:port'. The shortest format can be:" +
                           " 'userName@host'. For example: 'admin:pass@domain.com:5021' or 'admin@domain.com' with " +
                           "default port 21 and without password.")
  private ConnectionConfig connectionConfig;

  @Parameter(names = SOURCE, order = 3, description = "Source or local file.")
  private String source;

  @Parameter(names = DESTINATION, order = 4, description = "Destination file.")
  private String destination;

  @Parameter(names = BUFFER_SIZE,
             order = 5,
             converter = IntegerConverter.class,
             validateWith = CommonValidator.class,
             description = "Custom buffer size for uploading and downloading files. The size of the buffer should " +
                           "be increased by file size. For small files, just use 8128. For large files from " +
                           "hundreds of MB use at least 32768. By default, dynamic buffer calculation is used.")
  private Integer bufferSize;

  @Parameter(names = FILE_TYPE,
             order = 6,
             converter = FileTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "File type. Ensures the correct file transfer type.")
  private FileType fileType = FileType.binary;

  @Parameter(names = TRANSFER_MODE,
             order = 7,
             converter = TransferModeTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "Transfer mode.")
  private TransferModeType transferMode = TransferModeType.stream;

  @Parameter(names = FILE_STRUCTURE,
             order = 8,
             converter = FileStructureTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "File structure. It is used for specific purposes.")
  private FileStructureType fileStructure;

  @Parameter(names = CHARSET, order = 20, validateWith = CommonValidator.class, description = "Custom charset.")
  private String charset = ClientConfig.DEFAULT_CHARSET;

  @Parameter(names = SLIM,
             order = 21,
             validateWith = CommonValidator.class,
             converter = BooleanConverter.class,
             description = "Enables slim mode. In this mode, only general information is displayed. Everything else is concealed in the console.")
  private boolean slim;

  @Parameter(names = SILENT,
             order = 22,
             validateWith = CommonValidator.class,
             converter = BooleanConverter.class,
             description = "Silent mode. Only potential errors are written to the console.")
  private boolean silent;

  @Parameter(names = PASSIVE,
             order = 23,
             validateWith = CommonValidator.class,
             converter = BooleanConverter.class,
             description = "Connects to FTP in passive mode.")
  private boolean passive = true;

  @Parameter(names = HELP, help = true, order = 30, description = "Displays this help.")
  private boolean help;

  public List<String> getParameters() {
    return parameters;
  }

  public MethodType getMethod() {
    return method;
  }

  public ConnectionConfig getConnectionConfig() {
    return connectionConfig;
  }

  public String getSource() {
    return source;
  }

  public String getDestination() {
    return destination;
  }

  public Integer getBufferSize() {
    return bufferSize;
  }

  public FileType getFileType() {
    return fileType;
  }

  public TransferModeType getTransferMode() {
    return transferMode;
  }

  public FileStructureType getFileStructure() {
    return fileStructure;
  }

  public String getCharset() {
    return charset;
  }

  public boolean isHelp() {
    return help;
  }

  public boolean isSlim() {
    return slim;
  }

  public boolean isPassive() {
    return passive;
  }

  public boolean isSilent() {
    return silent;
  }
}
