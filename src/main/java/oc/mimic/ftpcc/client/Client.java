package oc.mimic.ftpcc.client;

import oc.mimic.ftpcc.client.config.ClientConfig;
import oc.mimic.ftpcc.client.config.ConnectionConfig;
import oc.mimic.ftpcc.client.exception.InvalidCredentialsException;
import oc.mimic.ftpcc.client.exception.ServerConnectionException;
import oc.mimic.ftpcc.client.helper.PlaceholderParser;
import oc.mimic.ftpcc.client.helper.ProgressBarWrapper;
import oc.mimic.ftpcc.util.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Základní implementace FTP klienta.
 *
 * @author mimic
 */
abstract class Client {

  private static final int DEFAULT_TIMEOUT = 20000; // msec
  private static final int CONNECT_TIMEOUT = 20000; // msec
  private static final int DATA_TIMEOUT = 10000; // msec
  private static final int CONTROL_KEEP_ALIVE_TIMEOUT = 10000; // msec
  private static final String CONTROL_ENCODING = "UTF-8";

  protected final FTPClient _client;
  protected final PlaceholderParser _parser;
  protected final ProgressBarWrapper _progressBar;
  private boolean connected;

  protected Client() {
    _client = new FTPClient();
    _parser = new PlaceholderParser();
    _progressBar = new ProgressBarWrapper();
  }

  /**
   * Umožní nastavit tichý režim.
   *
   * @param silentMode True, pokud se má do konzole vypsat jen minimum.
   */
  public final void silentMode(boolean silentMode) {
    _progressBar.setSilentMode(silentMode);
  }

  /**
   * Pokusí se připojí k FTP serveru dle konfigurace.
   *
   * @param config Konfigurace klienta.
   *
   * @return True, pokud bylo vytvořeno nové připojení.
   */
  protected final boolean tryConnect(ClientConfig config) {
    if (connected) {
      applyDynamicConfiguration(config);
      return false;
    }
    try {
      Logger.info("Establishing a connection to the server");
      var port = getValidPort(config.getPort());
      applyStaticConfiguration();
      _client.connect(config.getHost(), port);

      var replyCode = _client.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        Logger.error("The server rejects the connection. The return code is:", replyCode);
        _client.disconnect();
        return false;
      }
      if (config.isPassiveMode()) {
        _client.enterLocalPassiveMode();
      }
      Logger.info("Logging on to the server");

      if (!_client.login(config.getUserName(), config.getPassword())) {
        var msg = Logger.error("Invalid credentials to connect to the server");
        throw new InvalidCredentialsException(msg);
      } else {
        Logger.info("The connection to the server was successful");
        connected = true;
        applyDynamicConfiguration(config);
        return true;
      }
    } catch (Exception e) {
      var msg = Logger.error("Unable to connect to server");
      throw new ServerConnectionException(msg, e);
    }
  }

  /**
   * Odhlásí se z FTP serveru a následně se odpojí ze serveru.
   */
  public final void logout() {
    if (!connected) {
      return;
    }
    try {
      if (_client.logout()) {
        Logger.info("Logging out of the server was successful");
      }
    } catch (IOException e) {
      Logger.error("There was an error logging out of the server. Message: {0}", e.getMessage());
    } finally {
      closeConnection();
    }
  }

  /**
   * Vynutí odpojení z FTP serveru bez odhlášení.
   */
  public final void closeConnection() {
    if (!connected) {
      return;
    }
    try {
      _client.disconnect();
      Logger.info("Disconnection from the server was successful");
    } catch (IOException e) {
      Logger.error("There was an error disconnecting from the server. Message: {0}", e.getMessage());
    } finally {
      connected = false;
    }
  }

  /**
   * Statická konfigurace klienta, kterou nelze měnit.
   */
  private void applyStaticConfiguration() {
    _client.setDefaultTimeout(DEFAULT_TIMEOUT);
    _client.setConnectTimeout(CONNECT_TIMEOUT);
    _client.setControlEncoding(CONTROL_ENCODING);
    _client.setControlKeepAliveTimeout(CONTROL_KEEP_ALIVE_TIMEOUT);
    _client.setDataTimeout(DATA_TIMEOUT);
  }

  /**
   * Dynamická konfigurace, která se aplikuje při každém připojení k serveru nebo pokusu o vytvoření spojení. Tato
   * konfigurace se aplikuje pouze na aktivní připojení.
   *
   * @param config Konfigurace klienta.
   */
  private void applyDynamicConfiguration(ClientConfig config) {
    try {
      if (_client.isConnected()) {
        if (config.getBufferSize() != null) {
          _client.setBufferSize(config.getBufferSize());
        }
        if (config.getFileType() != null) {
          _client.setFileType(config.getFileType().getValue());
        }
        if (config.getFileStructure() != null) {
          _client.setFileStructure(config.getFileStructure().getValue());
        }
        if (config.getTransferMode() != null) {
          _client.setFileTransferMode(config.getTransferMode().getValue());
        }
        if (config.getCharset() != null) {
          _client.setCharset(Charset.forName(config.getCharset()));
        }
      }
    } catch (IOException e) {
      Logger.error(
              "There was an error applying the server connection configuration. Probably the connection to the server was lost");
    }
  }

  private static int getValidPort(Integer port) {
    if (port == null || port < 1 || port > 65535) {
      Logger.warning("The input port is not valid. Default will be used");
      return ConnectionConfig.DEFAULT_PORT;
    }
    return port;
  }
}
