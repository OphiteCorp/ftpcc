package oc.mimic.ftpcc.client.exception;

/**
 * Obecná vyjímka klienta.
 *
 * @author mimic
 */
public abstract class ClientException extends RuntimeException {

  public ClientException(String message) {
    super(message);
  }

  public ClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
