package oc.mimic.ftpcc.client.exception;

/**
 * Vstupní soubor není platný.
 *
 * @author mimic
 */
public final class InvalidInputFileException extends ClientException {

  public InvalidInputFileException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidInputFileException(String message) {
    super(message);
  }
}
