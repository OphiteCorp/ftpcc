package oc.mimic.ftpcc.client.exception;

/**
 * Nastala chyba při navázání připojení k FTP serveru.
 *
 * @author mimic
 */
public final class ServerConnectionException extends ClientException {

  public ServerConnectionException(String message, Throwable cause) {
    super(message, cause);
  }
}
