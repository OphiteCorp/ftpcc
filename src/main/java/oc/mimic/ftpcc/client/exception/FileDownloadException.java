package oc.mimic.ftpcc.client.exception;

/**
 * Nastala chyba při stahování souboru ze serverz.
 *
 * @author mimic
 */
public final class FileDownloadException extends ClientException {

  public FileDownloadException(String message, Throwable cause) {
    super(message, cause);
  }
}
