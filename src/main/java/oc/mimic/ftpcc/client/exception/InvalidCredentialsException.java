package oc.mimic.ftpcc.client.exception;

/**
 * Neplatné přihlašovací údaje pro připojení k FTP serveru.
 *
 * @author mimic
 */
public final class InvalidCredentialsException extends ClientException {

  public InvalidCredentialsException(String message) {
    super(message);
  }
}
