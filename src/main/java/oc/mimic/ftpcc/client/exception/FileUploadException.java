package oc.mimic.ftpcc.client.exception;

/**
 * Nastala chyba při nahrávání souboru na server.
 *
 * @author mimic
 */
public final class FileUploadException extends ClientException {

  public FileUploadException(String message, Throwable cause) {
    super(message, cause);
  }
}
