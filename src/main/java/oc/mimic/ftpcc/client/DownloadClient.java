package oc.mimic.ftpcc.client;

import oc.mimic.ftpcc.client.config.DownloadClientConfig;
import oc.mimic.ftpcc.client.exception.FileDownloadException;
import oc.mimic.ftpcc.util.Logger;
import oc.mimic.ftpcc.util.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Klient pro stažení souboru z FTP serveru.
 *
 * @author mimic
 */
public final class DownloadClient extends Client {

  public DownloadClient() {
    super();
  }

  /**
   * Stáhne soubor z FTP serveru.
   *
   * @param config     Konfigurace pro stažení souboru.
   * @param sourceFile Soubor, který se má stáhnout.
   *
   * @return Výstupní soubor.
   */
  public File download(DownloadClientConfig config, String sourceFile) {
    return download(config, sourceFile, null);
  }

  /**
   * Stáhne soubor z FTP serveru.
   *
   * @param config         Konfigurace pro stažení souboru.
   * @param sourceFile     Soubor, který se má stáhnout.
   * @param outputFilePath Výstupní soubor (kam se má uložit).
   *
   * @return Výstupní soubor.
   */
  public File download(DownloadClientConfig config, String sourceFile, String outputFilePath) {
    var source = new File(sourceFile);

    if (outputFilePath == null) {
      outputFilePath = source.getName();
    } else if (outputFilePath.endsWith("/") || outputFilePath.endsWith("\\")) {
      outputFilePath += source.getName();
    }
    if (config.isNewConnectionForEveryFile()) {
      logout();
    }
    tryConnect(config);

    try {
      Logger.info("Getting file ''{0}'' information from the server", sourceFile);

      var ftpSource = _client.mlistFile(sourceFile);

      if (ftpSource == null) {
        Logger.error("The file ''{0}'' does not exist on the server", sourceFile);
        return null;
      }
      outputFilePath = _parser.parseFromSourceFile(outputFilePath, source);
      outputFilePath = _parser.parseCustom(outputFilePath);

      var fileSize = ftpSource.getSize();
      var destination = new File(outputFilePath);
      var parent = destination.getParentFile();

      if (parent != null && !parent.exists()) {
        if (parent.mkdirs()) {
          Logger.info("Directory structure was created");
        }
      }
      if (config.getBufferSize() == null) {
        var bufferSize = Utils.calculateBufferSize(fileSize);
        Logger.info("Buffer size was automatically calculated to: {0} bytes", String.valueOf(bufferSize));
        _client.setBufferSize(bufferSize);
      }
      Logger.info("Starting download file ''{0}'' ({1}) from server", sourceFile,
              Utils.humanReadableFileSize(fileSize));

      var downloadMsg = "Downloading...";

      try (var stream = new BufferedOutputStream(new FileOutputStream(destination))) {
        _progressBar.start(fileSize);
        _client.setCopyStreamListener(new ClientProcessHandler(fileSize, _progressBar, downloadMsg));

        if (_client.retrieveFile(sourceFile, stream)) {
          _progressBar.done(fileSize, downloadMsg.length());
          Logger.info("The file was successfully downloaded from the server");
        } else {
          _progressBar.fail();
          _progressBar.done(fileSize, downloadMsg.length());
          var passiveMsg = !config.isPassiveMode() ? ". Have you forgotten to turn on passive mode?" : "";
          Logger.error("An error occurred and the file was not downloaded from the server" + passiveMsg);
        }
      } catch (Exception e) {
        _progressBar.done(fileSize, downloadMsg.length());
        throw e;
      }
      return destination;

    } catch (Exception e) {
      var msg = Logger.error("An error occurred while downloading the file from the server. The connection will be " +
                             "terminated");
      closeConnection();
      throw new FileDownloadException(msg, e);
    }
  }
}
