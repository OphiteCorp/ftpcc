package oc.mimic.ftpcc.client;

import oc.mimic.ftpcc.util.ProgressBar;
import org.apache.commons.net.io.CopyStreamAdapter;

/**
 * handler pro zpracování nahrávání nebo stahování souboru ze serveru.
 *
 * @author mimic
 */
final class ClientProcessHandler extends CopyStreamAdapter {

  private final ProgressBar progressBar;
  private final String progressName;
  private final long fileSize;

  public ClientProcessHandler(long fileSize, ProgressBar progressBar, String progressName) {
    this.fileSize = fileSize;
    this.progressBar = progressBar;
    this.progressName = progressName;
  }

  @Override
  public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
    progressBar.update(totalBytesTransferred, fileSize, progressName);
  }
}
