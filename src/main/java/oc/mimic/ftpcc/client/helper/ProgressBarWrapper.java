package oc.mimic.ftpcc.client.helper;

import oc.mimic.ftpcc.util.ProgressBar;

/**
 * Wrapper pro progress bar.
 *
 * @author mimic
 */
public final class ProgressBarWrapper extends ProgressBar {

  private boolean silentMode;

  @Override
  public void start(long total) {
    if (!silentMode) {
      super.start(total);
    }
  }

  public boolean isSilentMode() {
    return silentMode;
  }

  public void setSilentMode(boolean silentMode) {
    this.silentMode = silentMode;
  }

  @Override
  public void update(long done, long total) {
    if (!silentMode) {
      super.update(done, total);
    }
  }

  @Override
  public void update(long done, long total, String progressName) {
    if (!silentMode) {
      super.update(done, total, progressName);
    }
  }

  @Override
  public void done(long total, int prevMsgLength, String doneMessage) {
    if (!silentMode) {
      super.done(total, prevMsgLength, doneMessage);
    }
  }

  @Override
  public void done(long total, int prevMsgLength) {
    if (!silentMode) {
      super.done(total, prevMsgLength);
    }
  }

  @Override
  public void done(long total) {
    if (!silentMode) {
      super.done(total);
    }
  }

  @Override
  public void fail(String failMessage) {
    if (!silentMode) {
      super.fail(failMessage);
    }
  }

  @Override
  public void fail() {
    if (!silentMode) {
      super.fail();
    }
  }
}
