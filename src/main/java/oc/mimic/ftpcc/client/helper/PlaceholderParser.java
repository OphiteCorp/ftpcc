package oc.mimic.ftpcc.client.helper;

import oc.mimic.ftpcc.util.Utils;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Parsuje řetězec a nahrazuje ho za jiné hodnoty.
 *
 * @author mimic
 */
public final class PlaceholderParser {

  private static final SimpleDateFormat DATETIME = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
  private static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");

  /**
   * Parsuje řetězec.
   *
   * @param value Vstupní hodnota.
   *
   * @return Upravená vstupní hodnota.
   */
  public String parseCustom(String value) {
    if (value != null) {
      value = tryReplace(value, "uuid", () -> UUID.randomUUID().toString().replaceAll("-", ""));
      value = tryReplace(value, "mtime", () -> String.valueOf(System.currentTimeMillis()));
      value = tryReplace(value, "date", () -> DATE.format(new Date()));
      value = tryReplace(value, "datetime", () -> DATETIME.format(new Date()));
      value = tryReplace(value, "rand-number",
              () -> String.valueOf(ThreadLocalRandom.current().nextInt(100000, 1000000)));
    }
    return value;
  }

  /**
   * Parsuje řetězec.
   *
   * @param value      Vstupní hodnota.
   * @param sourceFile Zdrojový soubor.
   *
   * @return Upravená vstupní hodnota.
   */
  public String parseFromSourceFile(String value, File sourceFile) {
    if (value != null && sourceFile != null) {
      final var sourceName = Utils.removeFileExtension(sourceFile.getName());

      value = tryReplace(value, "name", () -> sourceName);
      value = tryReplace(value, "ext", () -> Utils.getFileExtension(sourceFile.getName()));
      value = tryReplace(value, "lower-ext", () -> Utils.getFileExtension(sourceFile.getName()).toLowerCase());
      value = tryReplace(value, "upper-name", sourceName::toUpperCase);
      value = tryReplace(value, "lower-name", sourceName::toLowerCase);
      value = tryReplace(value, "chinese-name", () -> {
        var nameLength = sourceName.length();
        var newName = new StringBuilder();

        for (var i = 0; i < nameLength; i++) {
          var c = (char) ThreadLocalRandom.current().nextInt(0x2B740, 0x2CEAF + 1);
          newName.append(c);
        }
        return newName.toString();
      });
      value = tryReplace(value, "base64-name", () -> Base64.getEncoder().encodeToString(sourceName.getBytes()));
      value = tryReplace(value, "url-encode-name", () -> URLEncoder.encode(sourceName, StandardCharsets.UTF_8));
    }
    return value;
  }

  private static String tryReplace(String value, String key, ReplaceAction action) {
    if (value.contains("[" + key + "]")) {
      var newValue = action.getNewValue();
      value = value.replaceAll("\\[" + key + "]", newValue);
    }
    return value;
  }

  private interface ReplaceAction {

    String getNewValue();
  }
}
