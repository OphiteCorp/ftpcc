package oc.mimic.ftpcc.client;

import oc.mimic.ftpcc.client.config.UploadClientConfig;
import oc.mimic.ftpcc.client.exception.FileUploadException;
import oc.mimic.ftpcc.client.exception.InvalidInputFileException;
import oc.mimic.ftpcc.util.Logger;
import oc.mimic.ftpcc.util.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Klient pro nahrání souboru na FTP server.
 *
 * @author mimic
 */
public final class UploadClient extends Client {

  public UploadClient() {
    super();
  }

  /**
   * Nahraje soubor na server.
   *
   * @param config    Konfigurace pro nahrání souboru na server.
   * @param inputFile Soubor, který se má nahrát na server.
   */
  public void upload(UploadClientConfig config, String inputFile) {
    upload(config, inputFile, null);
  }

  /**
   * Nahraje soubor na server.
   *
   * @param config         Konfigurace pro nahrání souboru na server.
   * @param inputFile      Soubor, který se má nahrát na server.
   * @param outputFilePath Cesta souboru na serveru. Pokud bude null, tak se použije název vstupního souboru.
   */
  public void upload(UploadClientConfig config, String inputFile, String outputFilePath) {
    var source = verifyFile(inputFile);

    if (outputFilePath == null) {
      outputFilePath = source.getName();
    } else if (outputFilePath.endsWith("/") || outputFilePath.endsWith("\\")) {
      outputFilePath += source.getName();
    }
    if (config.isNewConnectionForEveryFile()) {
      logout();
    }
    tryConnect(config);

    if (config.getBufferSize() == null) {
      var bufferSize = Utils.calculateBufferSize(source.length());
      Logger.info("Buffer size was automatically calculated to: {0} bytes", String.valueOf(bufferSize));
      _client.setBufferSize(bufferSize);
    }
    Logger.info("Starting upload file ''{0}'' ({1}) to server", source.getPath(),
            Utils.humanReadableFileSize(source.length()));

    var uploadMsg = "Uploading...";

    try (var stream = new BufferedInputStream(new FileInputStream(source))) {
      outputFilePath = _parser.parseFromSourceFile(outputFilePath, source);
      outputFilePath = _parser.parseCustom(outputFilePath);

      var outFile = new File(outputFilePath);

      if (outFile.getParent() != null) {
        _client.makeDirectory(outFile.getParent());
        _client.changeWorkingDirectory(outFile.getParent());
      }
      _progressBar.start(source.length());
      _client.setCopyStreamListener(new ClientProcessHandler(source.length(), _progressBar, uploadMsg));

      if (_client.storeFile(outFile.getName(), stream)) {
        _progressBar.done(source.length(), uploadMsg.length());
        Logger.info("The file was successfully uploaded to the server");
      } else {
        _progressBar.fail();
        _progressBar.done(source.length(), uploadMsg.length());
        var passiveMsg = !config.isPassiveMode() ? ". Have you forgotten to turn on passive mode?" : "";
        Logger.error("An error occurred and the file was not stored to the server" + passiveMsg);
      }
    } catch (Exception e) {
      _progressBar.done(source.length(), uploadMsg.length());
      var msg = Logger.error("An error occurred while uploading the file to the server. The connection will be " +
                             "terminated");
      closeConnection();
      throw new FileUploadException(msg, e);
    }
  }

  /**
   * Ověří vstupní soubor, zda existuje a je možné ho číst.
   *
   * @param inputFile Vstupní soubor, který se má nahrát na server.
   */
  private static File verifyFile(String inputFile) {
    try {
      Logger.info("Verification of the input file: {0}", inputFile);

      if (inputFile == null) {
        var msg = Logger.error("The input file was not filled");
        throw new InvalidInputFileException(msg);
      }
      var file = new File(inputFile);

      if (!file.exists()) {
        var msg = Logger.error("File does not exist: {0}", inputFile);
        throw new InvalidInputFileException(msg);

      } else if (!file.isFile() || !file.canRead()) {
        var msg = Logger.error("The input file is not a file or can not be read: {0}", inputFile);
        throw new InvalidInputFileException(msg);
      }
      Logger.info("File is verified: {0}", inputFile);
      return file;

    } catch (Exception e) {
      var msg = Logger.error("Unable to verify input file: {0}", inputFile);
      throw new InvalidInputFileException(msg, e);
    }
  }
}
