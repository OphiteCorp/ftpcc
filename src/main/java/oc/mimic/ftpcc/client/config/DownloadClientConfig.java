package oc.mimic.ftpcc.client.config;

/**
 * Konfigurace pro stažení souboru z FTP serveru.
 *
 * @author mimic
 */
public final class DownloadClientConfig extends ClientConfig {

  private CommonConfig common;
  private boolean newConnectionForEveryFile;

  public DownloadClientConfig() {
  }

  public DownloadClientConfig(ClientConfig config) {
    super(config);
  }

  public DownloadClientConfig(DownloadClientConfig config) {
    super(config);

    if (config != null) {
      newConnectionForEveryFile = config.newConnectionForEveryFile;
    }
  }

  public boolean isNewConnectionForEveryFile() {
    return newConnectionForEveryFile;
  }

  public void setNewConnectionForEveryFile(boolean newConnectionForEveryFile) {
    this.newConnectionForEveryFile = newConnectionForEveryFile;
  }

  public CommonConfig getCommon() {
    return common;
  }

  public void setCommon(CommonConfig common) {
    this.common = common;
  }
}
