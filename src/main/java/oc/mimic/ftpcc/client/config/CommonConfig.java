package oc.mimic.ftpcc.client.config;

/**
 * Běžný nastavení všech implementací klientů. Toto nastavení nemá přímo s klientem nic společného.
 *
 * @author mimic
 */
public final class CommonConfig {

  private boolean slimMode;
  private boolean silentMode;

  public boolean isSlimMode() {
    return slimMode;
  }

  public void setSlimMode(boolean slimMode) {
    this.slimMode = slimMode;
  }

  public boolean isSilentMode() {
    return silentMode;
  }

  public void setSilentMode(boolean silentMode) {
    this.silentMode = silentMode;
  }
}
