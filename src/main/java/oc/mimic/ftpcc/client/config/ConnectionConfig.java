package oc.mimic.ftpcc.client.config;

/**
 * Konfigurace pro připojení k FTP serveru.
 *
 * @author mimic
 */
public class ConnectionConfig {

  public static final int DEFAULT_PORT = 21;

  private String host;
  private Integer port = DEFAULT_PORT;
  private String userName;
  private String password;

  public ConnectionConfig() {
  }

  public ConnectionConfig(ConnectionConfig config) {
    if (config != null) {
      host = config.host;
      port = config.port;
      userName = config.userName;
      password = config.password;
    }
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
