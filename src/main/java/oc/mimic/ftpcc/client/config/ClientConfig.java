package oc.mimic.ftpcc.client.config;

import oc.mimic.ftpcc.param.type.FileStructureType;
import oc.mimic.ftpcc.param.type.FileType;
import oc.mimic.ftpcc.param.type.TransferModeType;

import java.nio.charset.StandardCharsets;

/**
 * Základní konfigurace klienta.
 *
 * @author mimic
 */
public class ClientConfig extends ConnectionConfig {

  public static final String DEFAULT_CHARSET = StandardCharsets.UTF_8.name();

  private Integer bufferSize;
  private FileType fileType = FileType.binary;
  private TransferModeType transferMode = TransferModeType.stream;
  private FileStructureType fileStructure; // není potřeba nastavovat výchozí hodnotu
  private String charset = DEFAULT_CHARSET;
  private boolean passiveMode;

  public ClientConfig() {
  }

  public ClientConfig(ConnectionConfig config) {
    super(config);
  }

  public ClientConfig(ClientConfig config) {
    super(config);

    if (config != null) {
      bufferSize = config.bufferSize;
      fileType = config.fileType;
      charset = config.charset;
      fileStructure = config.fileStructure;
      transferMode = config.transferMode;
      passiveMode = config.passiveMode;
    }
  }

  public Integer getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(Integer bufferSize) {
    this.bufferSize = bufferSize;
  }

  public FileType getFileType() {
    return fileType;
  }

  public void setFileType(FileType fileType) {
    this.fileType = fileType;
  }

  public String getCharset() {
    return charset;
  }

  public void setCharset(String charset) {
    this.charset = charset;
  }

  public FileStructureType getFileStructure() {
    return fileStructure;
  }

  public void setFileStructure(FileStructureType fileStructure) {
    this.fileStructure = fileStructure;
  }

  public TransferModeType getTransferMode() {
    return transferMode;
  }

  public void setTransferMode(TransferModeType transferMode) {
    this.transferMode = transferMode;
  }

  public boolean isPassiveMode() {
    return passiveMode;
  }

  public void setPassiveMode(boolean passiveMode) {
    this.passiveMode = passiveMode;
  }
}
