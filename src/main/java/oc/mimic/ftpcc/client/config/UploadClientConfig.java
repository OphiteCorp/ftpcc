package oc.mimic.ftpcc.client.config;

/**
 * Konfigurace pro nahrání souboru na FTP server.
 *
 * @author mimic
 */
public final class UploadClientConfig extends ClientConfig {

  private CommonConfig common;
  private boolean newConnectionForEveryFile;

  public UploadClientConfig() {
  }

  public UploadClientConfig(ClientConfig config) {
    super(config);
  }

  public UploadClientConfig(UploadClientConfig config) {
    super(config);

    if (config != null) {
      newConnectionForEveryFile = config.newConnectionForEveryFile;
    }
  }

  public boolean isNewConnectionForEveryFile() {
    return newConnectionForEveryFile;
  }

  public void setNewConnectionForEveryFile(boolean newConnectionForEveryFile) {
    this.newConnectionForEveryFile = newConnectionForEveryFile;
  }

  public CommonConfig getCommon() {
    return common;
  }

  public void setCommon(CommonConfig common) {
    this.common = common;
  }
}
